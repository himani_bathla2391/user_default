//
//  AppDelegate.h
//  User_Default
//
//  Created by Clicklabs 104 on 11/19/15.
//  Copyright © 2015 cl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

