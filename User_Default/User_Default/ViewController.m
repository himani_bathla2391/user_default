//
//  ViewController.m
//  User_Default
//
//  Created by Clicklabs 104 on 11/19/15.
//  Copyright © 2015 cl. All rights reserved.
//

#import "ViewController.h"
NSMutableArray *data;
@interface ViewController ()

@end

@implementation ViewController
@synthesize usernameLabel;
@synthesize passwordLabel;
@synthesize usernameTextfield;
@synthesize passwordTextfield;
@synthesize next;
@synthesize image1;
- (void)viewDidLoad {
    [super viewDidLoad];
    passwordTextfield.secureTextEntry= YES;
    [passwordTextfield resignFirstResponder];
    [usernameTextfield resignFirstResponder];
    // Do any additional setup after loading the view, typically from a nib.
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"fetchUsername"]) {
        data=[[NSMutableArray alloc]init];
        [data addObject:self.usernameTextfield.text];
        [data addObject:self.passwordTextfield.text];
        [[NSUserDefaults standardUserDefaults]setObject:data forKey:@"details"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
