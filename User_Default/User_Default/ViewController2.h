//
//  ViewController2.h
//  User_Default
//
//  Created by Clicklabs 104 on 11/19/15.
//  Copyright © 2015 cl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController2 : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *getUsername;
@property (weak, nonatomic) IBOutlet UIButton *next2;
@property (weak, nonatomic) IBOutlet UIImageView *image2;
@end
