//
//  ViewController.h
//  User_Default
//
//  Created by Clicklabs 104 on 11/19/15.
//  Copyright © 2015 cl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController2.h"
#import "ViewController3.h"

@interface ViewController : UIViewController


@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *passwordLabel;
@property (weak, nonatomic) IBOutlet UITextField *usernameTextfield;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextfield;

@property (weak, nonatomic) IBOutlet UIButton *next;
@property (weak, nonatomic) IBOutlet UIImageView *image1;

@end

